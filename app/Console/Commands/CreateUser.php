<?php

namespace App\Console\Commands;

use App\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class CreateUser extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'admin:create ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create Admin User';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $name=$this->ask('Enter the name : ');
        $email=$this->getEmailInput();
        $password=$this->getPasswordInput();
        User::create(['name'=>$name,'email'=>$email,'password'=>Hash::make($password)]);
        $this->info("Admin '{$email}' create success!");
    }

    private function getEmailInput(){
        $email = $this->ask('Enter the admin email address : ');
        $response_value=$this->isValid('email','required|email|unique:users',$email);
        if($response_value){
            $this->error($response_value);
            $this->getEmailInput();
        }
        return $email;
    }

    private function getPasswordInput(){
        $password = $this->secret('Enter the password (1 Capital Letter, 1 number, 1 Special character [*-+=^%$#@.<>?]): ');
        $response_value=$this->isValid('password','required|regex:/^.*(?=.{3,})(?=.*[A-Z])(?=.*[0-9])(?=.*[*-+=^%$#@.<>?]).*$/',$password);
        if($response_value){
            $this->error($response_value);
            $this->getPasswordInput();
        }
        return $password;
    }

    private function isValid($field,$rule,$value){
        $validator = Validator::make(
            [
                $field => $value,
            ],
            [
                $field =>$rule ,
            ]
        );

        if($validator->fails()){
            return $validator->errors()->first($field);
        }else{
            return null;
        }
    }
}
