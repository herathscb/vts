<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Document extends Model
{
    public $table = 'documents';
    protected $primaryKey = 'document_id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    /*
     * document_name = Name of the document
     * document_code = Code of the document
     * version_number = version_number of the document
     * created_by = Created By (user_id)
     * updated_by = Update By (user_id)
     */
    public $fillable=['document_name','document_code','version_number', 'created_by'];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'document_name' => 'required',
        'document_code' => 'required',
        'version_number' => 'required',
    ];


    public function createdBy()
    {
        return $this->belongsTo('App\User','created_by','id');
    }

    public function updatedBy()
    {
        return $this->belongsTo('App\User','updated_by','id');
    }



}
