<?php

namespace App\Http\Controllers\Api;

use App\Document;
use App\Http\Controllers\Controller;
use App\Traits\ApiResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Validator;

class DocumentController extends Controller
{
    use ApiResponse;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Gate::any(['isAdmin','isUser'])) {
            $documents = Document::get();
            return $this->sendResponse($documents, 'Document load success');
        }else{
            return $this->sendAccessDenied();
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (Gate::allows('isAdmin')) {
            $input=$request->all();
            try {
                $validator = Validator::make($input, Document::$rules);
                if ($validator->fails()) {
                    return $this->sendError('Validation Failed', $validator->messages());
                } else {
                    $document = Document::create($input);
                    return $this->sendResponse($document, 'Document save success');
                }
            } catch (\Exception $e) {
                return $this->sendError('Validation Failed', $e->getMessage());
            }
        }else{
            return $this->sendAccessDenied();
        }


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (Gate::any(['isAdmin','isUser'])) {
            $row=Document::find($id);
            if(isset($row)){
                return $this->sendResponse($row,'Document load success');
            }else{
                return $this->sendError('Validation Failed','Document load failed');
            }
        }else{
            return $this->sendAccessDenied();
        }


    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (Gate::any(['isAdmin','isUser'])) {
            $input = $request->all();
            try {
                $validator = Validator::make($input, Document::$rules);
                if ($validator->fails()) {
                    return $this->sendError('Validation Failed', $validator->messages());
                } else {
                    $row=Document::find($id);
                    $row->document_name=$input['document_name'];
                    $row->document_code=$input['document_code'];
                    $row->version_number=$input['version_number'];
                    $row->save();
                    return $this->sendResponse($row,'Document update success');
                }
            } catch (\Exception  $e) {
                return $this->sendError('Update Failed',$e->getMessage());
            }
        }else{
            return $this->sendAccessDenied();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (Gate::allows('isAdmin')) {
            try {
                $status=Document::destroy($id);
                if($status==1){
                    return $this->sendResponse([],'Document delete success');
                }else{
                    return $this->sendError('Delete Failed','Document delete failed');
                }
            } catch (\Exception $e) {
                return $this->sendError('Delete Failed',$e->getMessage());
            }
        }else{
            return $this->sendAccessDenied();
        }


    }

    public function search (Request $request){
        $rows=Document::where('document_name', 'like', '%' . $request->document_name . '%')
            ->where('version_number', 'like', '%' . $request->version_number . '%')
            ->get();
        if(count($rows)>0){
            return $this->sendResponse($rows,'Document search success');
        }else{
            return $this->sendError('Document search failed','Document search failed');
        }
    }
}
