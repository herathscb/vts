<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Traits\ApiResponse;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{

    use ApiResponse;

    /**
     * Login process.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function login(Request $request){
        $user = User::where('email', $request->email)->first();
        if (! $user || ! Hash::check($request->password, $user->password)) {
            return response([
                'email' => ['The provided credentials are incorrect.'],
            ],404);
        }
        return $user->createToken('my-token')->plainTextToken;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (Gate::allows('isAdmin')) {
            $input=$request->all();
            try {
                $validator = Validator::make($input, User::$rules);
                if ($validator->fails()) {
                    return $this->sendError('Validation Failed', $validator->messages());
                } else {
                    User::create(['name'=>$request->name,'email'=>$request->email,'password'=>Hash::make($request->password)]);
                    return $this->sendResponse([], 'User create success');
                }
            } catch (\Exception $e) {
                return $this->sendError('Validation Failed', $e->getMessage());
            }
        }else{
            return $this->sendAccessDenied();
        }


    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update_password(Request $request)    {
        if (Gate::any(['isAdmin','isUser'])) {
            try {
                $validator = Validator::make([$request->password], ['password'=>'required|regex:/^.*(?=.{3,})(?=.*[A-Z])(?=.*[0-9])(?=.*[*-+=^%$#@.<>?]).*$/']);
//dd($validator->fails());
                if ($validator->fails()) {
                    return $this->sendError('Validation Failed', $validator->messages());
                } else {
                    $row=auth()->user();
                    $row->password=Hash::make($request->password);
                    $row->save();
                    return $this->sendResponse($row,'Password update success');
                }
            } catch (\Exception  $e) {
                return $this->sendError('Update Failed',$e->getMessage());
            }
        }else{
            return $this->sendAccessDenied();
        }


    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (Gate::allows('isAdmin')) {
            try {
                $status = User::destroy($id);
                if ($status == 1) {
                    return $this->sendResponse([], 'User delete success');
                } else {
                    return $this->sendError('Delete Failed', 'User delete failed');
                }
            } catch (\Exception $e) {
                return $this->sendError('Delete Failed', $e->getMessage());
            }
        }else{
            return $this->sendAccessDenied();
        }
    }



}
