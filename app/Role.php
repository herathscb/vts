<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    public $table = 'roles';
    protected $primaryKey = 'role_id';
    public $fillable=['role_name'];

    public function userRoles()
    {
        return $this->hasMany('App\UserRole','role_id','role_id');
    }

}
