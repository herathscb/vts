<?php
/**
 * Created by PhpStorm.
 * User: HP
 * Date: 11/16/2021
 * Time: 4:05 AM
 */

namespace App\Traits;

trait ApiResponse
{
    public static function sendResponse($result, $message)
    {
        $response = [
            'success' => true,
            'data' => $result,
            'message' => $message,
        ];
        return response()->json($response, 200);
    }

    public static function sendError($error, $errorMessages = [], $code = 200)
    {
        $response = [
            'success' => false,
            'message' => $error,
        ];
        if (!empty($errorMessages)) {
            $response['data'] = $errorMessages;
        }
        return response()->json($response, $code);
    }

    public static function sendAccessDenied()
    {
        $response = [
            'success' => false,
            'message' => 'You do not have permission for this',
        ];
        return response()->json($response, 200);
    }



}