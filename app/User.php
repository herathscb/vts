<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required',
        'email' => 'required|email|unique:users',
        'password' => 'required|regex:/^.*(?=.{3,})(?=.*[A-Z])(?=.*[0-9])(?=.*[*-+=^%$#@.<>?]).*$/',
    ];

    public function isAdmin(){
        $role_id=$this->userRole->role_id;
        if($role_id==1){
            return true;
        }else{
            return false;
        }
    }

    public function isUser(){
        $role_id=$this->userRole->role_id;
        if($role_id==2){
            return true;
        }else{
            return false;
        }
    }

    public function documentCreated()
    {
        return $this->hasMany('App\Document','created_by','id');
    }

    public function documentUpdated()
    {
        return $this->hasMany('App\Document','updated_by','id');
    }

    public function userRole()
    {
        return $this->belongsTo('App\UserRole','id','user_id');
    }





}
