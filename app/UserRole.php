<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserRole extends Model
{
    public $table = 'user_roles';
    protected $primaryKey = 'user_role_id';
    public $fillable=['user_id','role_id'];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'user_id' => 'required',
        'role_id' => 'required',
    ];

    public function user()
    {
        return $this->belongsTo('App\User','user_id','id');
    }

    public function role()
    {
        return $this->belongsTo('App\Role','role_id','role_id');
    }
}
