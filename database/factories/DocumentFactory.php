<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Document;
use Faker\Generator as Faker;

$factory->define(Document::class, function (Faker $faker) {
    return [
        'document_name' => $faker->name,
        'document_code' => strtoupper($faker->word),
        'version_number' => $faker->randomDigit,
        'created_by' => 1,
        'updated_by' => 0
    ];
});
