<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('login', 'Api\UserController@login');

Route::group(['middleware' => 'auth:sanctum'], function () {

    //Search document
    Route::post('document/search', 'Api\DocumentController@search')->name('document.search');
    /*
     *Create routes only for index,store, show, update and destroy
     */
    Route::apiResource('document', 'Api\DocumentController');
    /*
     *Create routes only for store, and destroy
     */
    Route::apiResource('user', 'Api\UserController', ['only' => ['store', 'destroy']]);

    /*
     *Create routes for update password
     */
    Route::patch('user/update-password', 'Api\UserController@update_password')
        ->name('user.update-password');

    /*
     *Create routes only for store
     */
    Route::apiResource('user', 'Api\UserController', ['only' => ['store', 'destroy']]);

    Route::apiResource('user-role', 'Api\UserRoleController', ['only' => ['store']]);

});

